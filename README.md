# mtg-standard-reprints

A simple web application for viewing reprinted cards in Magic the Gathering Standard format.
Allows a MtG player to quickly see what cards have been reprinted from which sets.

I started this project as a quick frontend app to try out static website hosting on
amazon s3 buckets. It's since turned into a bit of a playground to practice devops,
as this is the side of web development I'm weakest at.

It currently has a very rudimentary CI pipeline and a handful of unit tests. I won't
be adding features until I have it proper unit tests in place.

Current Pipeline:
 - Run Linters
 - Buid
 - Run unit tests

I plan on adding 'deploy to staging', 'run e2e against staging', and 'deploy to production'
stages to the pipeline to complete the full CI/CD cycle.


Uses the [Scryfall Api](https://scryfall.com/docs/api) for image links and card details.

Uses [MTGJSON](https://mtgjson.com/) for standard format card list.

## This project is now live!

It went live with comit e36713b2

Visit it over at [www.mtg-reprints.com](http://www.mtg-reprints.com)!



## Project Setup

### Clone and setup repository.
```
git clone https://gitlab.com/petermulligan88/mtg-standard-reprints.git
```
```
cd mtg-standard-reprints
```
```
npm install
```

### Compile, run, and provide hot-reloading for development. (Runs locally on port 8080)
```
npm run serve
```

### Compile and minifies for production.
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```
