/* eslint-disable no-plusplus */
import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    cards: [],
    standardSets: [
      'GRN', 'RNA', 'WAR', 'M20', 'ELD', 'THB',
      /* Pre-release sets get a P prefix. */
      'PGRN', 'PRNA', 'PWAR', 'PM20', 'PELD', 'PTHB',
      /* Promo Packs. */
      'MB1', 'PSLD',
    ],
  },
  mutations: {
    addCard: (state, card) => {
      state.cards.push(card);
    },
  },
  actions: {
    async fetchCards(context) {
      await axios.get('https://www.mtgjson.com/files/StandardCards.json')
        .then(async ({ data }) => {
          await Object.keys(data).forEach((card) => {
            /* Filter out all cards that only have prints in standard sets. */
            const filtered = data[card].printings.filter(
              set => !context.state.standardSets.includes(set),
            ); /* Filter out all basic lands. */
            if (filtered.length && data[card].name !== 'Mountain'
              && data[card].name !== 'Swamp' && data[card].name !== 'Plains'
              && data[card].name !== 'Island' && data[card].name !== 'Forest') {
              /* Add reprint to array. */
              context.commit('addCard', data[card]);
            }
          });
        })
        .catch(() => {
          // TODO: Handle 'unable to load'.
        });
    },
  },
  getters: {
    cards: state => state.cards,
  },
});
