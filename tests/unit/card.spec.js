import { shallowMount, createLocalVue } from '@vue/test-utils';
import Card from '../../src/components/Card.vue';

jest.mock('axios');

const localVue = createLocalVue();
let wrapper = {};
const methods = {
  getCard: jest.fn(() => true),
};
const propsData = {
  card: {
    name: 'Absorb',
    printings: ['RNA', 'PRNA', 'INV'],
  },
  details: null,
};

function shallowMountComponent() {
  return shallowMount(Card, {
    localVue,
    methods,
    propsData,
  });
}

describe('Card', () => {
  wrapper = shallowMountComponent();
  it('should receive props correctly', () => {
    expect(wrapper.props('card').name).toBe('Absorb');
    expect(wrapper.props('card').printings).toEqual(['RNA', 'PRNA', 'INV']);
  });
  it('should display the card props name', () => {
    expect(wrapper.find('[data-test="name"]').text()).toBe(wrapper.props('card').name);
  });
  it('should display the correct nymber of reprints', () => {
    expect(parseInt(wrapper.find('[data-test="reprints"]').text(), 10))
      .toBe(wrapper.props('card').printings.length);
  });
});

describe('methods', () => {
  let spy;
  it('should call created', () => {
    spy = jest.spyOn(Card, 'created');
    wrapper = shallowMountComponent();
    expect(spy).toHaveBeenCalled();
    spy.mockReset();
  });
});
