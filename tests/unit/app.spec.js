import { shallowMount, createLocalVue } from '@vue/test-utils';
import VueRouter from 'vue-router';
import App from '../../src/App.vue';

describe('App', () => {
  const localVue = createLocalVue();
  localVue.use(VueRouter);
  const wrapper = shallowMount(App, {
    localVue,
  });
  it('should have exactly 1 element named "app" to mount on', () => {
    expect(wrapper.findAll('[data-test="app"]').wrappers.length).toBe(1);
  });
});
